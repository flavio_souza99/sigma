﻿using frmLogin.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Orçamento
{
    class OrcamentoItemDataBase
    {
        public int Salvar(OrcamentoItemDTO dto)
        {
            string script = @"INSERT INTO tb_pedido_item (id_orcamneto, id_carro) VALUES (@id_orcamneto, @id_carro)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_orcamento", dto.IdOrcamento));
            parms.Add(new MySqlParameter("id_carro", dto.IdCarro));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
    }
}
