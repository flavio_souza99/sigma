﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Orçamento
{
    class OrcamentoBusiness
    {
        public int Salvar(OrcamentoDTO dto)
        {
            OrcamentoDataBase db = new OrcamentoDataBase();
            return db.Salvar(dto);
        }

        public void Alterar(OrcamentoDTO dto)
        {
            OrcamentoDataBase db = new OrcamentoDataBase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            OrcamentoDataBase db = new OrcamentoDataBase();
            db.Remover(id);
        }

        public List<OrcamentoConsultarView> Consultar(string produto)
        {
            OrcamentoDataBase db = new OrcamentoDataBase();
            return db.Consultar(produto);
        }

        public List<OrcamentoDTO> Listar()
        {
            OrcamentoDataBase db = new OrcamentoDataBase();
            return db.Listar();
        }
    }
}
