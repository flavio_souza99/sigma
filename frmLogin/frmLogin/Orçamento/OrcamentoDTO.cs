﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Orçamento
{
    class OrcamentoDTO
    {
        public int Id { get; set; }
        public int CarroId { get; set; }
        public int ClienteId { get; set; }
        public string Valor1 { get; set; }
        public string Valor2 { get; set; }
        public string Valor3 { get; set; }
        public string Defeito { get; set; }
    }
}
