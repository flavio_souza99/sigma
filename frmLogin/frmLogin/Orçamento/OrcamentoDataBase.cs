﻿using frmLogin.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Orçamento
{
    class OrcamentoDataBase
    {
        public int Salvar(OrcamentoDTO dto)
        {
            string script = @"INSERT INTO tb_pedido (id_carro, id_cliente, nm_valor1, nm_valor2, nm_valor3, nm_defeito) VALUES (@id_carro, @id_cliente, @nm_valor1, @nm_valor2, @nm_valor3, @nm_defeito)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.ClienteId));
            parms.Add(new MySqlParameter("id_carro", dto.CarroId));
            parms.Add(new MySqlParameter("nm_valor1", dto.Valor1));
            parms.Add(new MySqlParameter("nm_valor2", dto.Valor2));
            parms.Add(new MySqlParameter("nm_valor3", dto.Valor3));
            parms.Add(new MySqlParameter("nm_defeito", dto.Defeito));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(OrcamentoDTO dto)
        {
            string script = @"UPDATE tb_produto 
                                 SET id_carro = @id_carro,
                                     nm_valor1 = @nm_valor1,
                                     nm_valor2 = @nm_valor2,
                                     nm_valor3 = @nm_valor3,
                                     nm_defeito = @nm_defeito
                               WHERE id_carro = @id_carro";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.ClienteId));
            parms.Add(new MySqlParameter("id_carro", dto.CarroId));
            parms.Add(new MySqlParameter("nm_valor1", dto.Valor1));
            parms.Add(new MySqlParameter("nm_valor2", dto.Valor2));
            parms.Add(new MySqlParameter("nm_valor3", dto.Valor3));
            parms.Add(new MySqlParameter("nm_defeito", dto.Defeito));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_pedido WHERE id_pedido = @id_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }


        public List<OrcamentoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<OrcamentoDTO> lista = new List<OrcamentoDTO>();
            while (reader.Read())
            {
                OrcamentoDTO dto = new OrcamentoDTO();
                parms.Add(new MySqlParameter("id_cliente", dto.ClienteId));
                parms.Add(new MySqlParameter("id_carro", dto.CarroId));
                parms.Add(new MySqlParameter("nm_valor1", dto.Valor1));
                parms.Add(new MySqlParameter("nm_valor2", dto.Valor2));
                parms.Add(new MySqlParameter("nm_valor3", dto.Valor3));
                parms.Add(new MySqlParameter("nm_defeito", dto.Defeito));

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<OrcamentoConsultarView> Consultar(string carro)
        {
            string script = @"SELECT * FROM vw_pedido_consultar WHERE nm_carro like @nm_carro";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_carro", carro + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<OrcamentoConsultarView> lista = new List<OrcamentoConsultarView>();
            while (reader.Read())
            {
                OrcamentoConsultarView dto = new OrcamentoConsultarView();
                dto.Id = reader.GetInt32("id_pedido");     
                dto.nome = reader.GetString("nm_nome");
                dto.carro = reader.GetString("nm_carro");
                dto.Valor1 = reader.GetString("nm_valor1");
                dto.Valor2 = reader.GetString("nm_valor2");
                dto.Valor3 = reader.GetString("nm_valor3");
                dto.Defeito = reader.GetString("nm_defeito");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
