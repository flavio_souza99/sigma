﻿using frmLogin.Adm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class frmAdm : Form
    {
        public frmAdm()
        {
            InitializeComponent();
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            frmlogar ir = new frmlogar();
            this.Hide();
            ir.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string a = txtChave.Text;

            string b = txtPasse.Text;
            AdmBussines business = new AdmBussines();
            bool logou = business.Logar(a, b);


            if (logou == true)
            {
                frmCadastrar ir = new frmCadastrar();
                this.Hide();
                ir.ShowDialog();

            }
            else
            {
                MessageBox.Show("Dados inválidos", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmlogar ir = new frmlogar();
            this.Hide();
            ir.ShowDialog();
        }

        private void txtChave_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtPasse_Click(object sender, EventArgs e)
        {
            if (txtPasse.Text == "00000000")
            {
                txtPasse.Text = string.Empty;
            }
        }

        private void txtChave_Click(object sender, EventArgs e)
        {
            if (txtChave.Text == "Digite")
            {
                txtChave.Text = string.Empty;
            }
        }
    }
}
