﻿using frmLogin.Login;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class frmCadastrar : Form
    {
        public frmCadastrar()
        {
            InitializeComponent();
        }

        private void frmCadastrar_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmMenu telapedido = new frmMenu();

            string nome = txtnome.Text.Trim();
            string senha = txtsenha.Text.Trim();
            string nome1 = txtnome1.Text.Trim();
            string rg1 = txtrg1.Text.Trim();
            string departamento1 = txtdepartamento1.Text.Trim();
            string sexo1 = txtsexo1.Text.Trim();
            string tel1 = txttel1.Text.Trim();
            string rua1 = txtrua1.Text.Trim();
            string cep1 = txtcep1.Text.Trim();
            DateTime nascimento1 = Convert.ToDateTime(txtnascimento1.Text);
            string bairro1 = txtbairro1.Text.Trim();
            string numero1 = txtnumero1.Text.Trim();

            if (nome == string.Empty || nome == "Crie um E-mail")
            {
                MessageBox.Show("O campo nome é obrigatorio", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;

            }

            if (senha == string.Empty || senha == "qualquer")
            {
                MessageBox.Show("O campo senha é obrigatorio", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            if (nome1 == string.Empty || nome1 == "Nome Completo")
            {
                MessageBox.Show("O campo nome é obrigatorio", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            if (rg1 == string.Empty || rg1 == "Rg" || txtrg1.Text.Length <=11)
            {
                MessageBox.Show("O campo RG é obrigatorio", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            if (departamento1 == string.Empty || departamento1 == "Departamento")
            {
                MessageBox.Show("O campo Departamneto é obrigatorio", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            if (sexo1 == string.Empty || sexo1 == "Sexo")
            {
                MessageBox.Show("O campo Sexo é obrigatorio", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            if (tel1 == string.Empty || tel1 == "Telefone" || txttel1.Text.Length <= 14)
            {
                MessageBox.Show("O campo celular é obrigatorio", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            if (rua1 == string.Empty || rua1 == "Sua rua")
            {
                MessageBox.Show("O campo Rua é obrigatorio", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            if (cep1 == string.Empty || cep1 == "Cep" || txtcep1.Text.Length <= 8)
            {
                MessageBox.Show("O campo Cep é obrigatorio", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

           

            if (bairro1 == string.Empty || bairro1 == "Seu bairro")
            {
                MessageBox.Show("O campo Bairro é obrigatorio", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            if (numero1 == string.Empty || numero1 == "Nº")
            {
                MessageBox.Show("O campo Numero é obrigatorio", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }


            loginDTO dados = new loginDTO();
            dados.nome = nome;
            dados.senha = senha;
            dados.nome1 = nome1;
            dados.rg1 = rg1;
            dados.departamento1 = departamento1;
            dados.sexo1 = sexo1;
            dados.tel1 = tel1;
            dados.rua1 = rua1;
            dados.cep1 = cep1;
            dados.nascimento1 = nascimento1;
            dados.bairro1 = bairro1;
            dados.numero1 = numero1;

            LoginBusiness salvardados = new LoginBusiness();
            salvardados.Salvar(dados);

            MessageBox.Show("Cadastrado");


            frmlogar voltar = new frmlogar();
            this.Hide();
            voltar.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmlogar ir = new frmlogar();
            this.Hide();
            ir.ShowDialog();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txtnome_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtnumero1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtcep1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtnome1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }

        }

        private void txtnome1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtsenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar) == true || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtsenha_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtrua1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtnome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(char.IsLetter(e.KeyChar) || char.IsPunctuation(e.KeyChar) || char.IsControl(e.KeyChar) == true)
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void kkk_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtnome_Click(object sender, EventArgs e)
        {
            if (txtnome.Text == "Crie um E-mail")
            {
                txtnome.Text = string.Empty;
            }
        }

        private void txtsenha_Click(object sender, EventArgs e)
        {
            if (txtsenha.Text == "qualquer")
            {
                txtsenha.Text = string.Empty;
            }
        }

        private void txtrua1_Click(object sender, EventArgs e)
        {
            if (txtrua1.Text == "Sua rua")
            {
                txtrua1.Text = string.Empty;
            }
        }

        private void txtbairro1_Click(object sender, EventArgs e)
        {
            if (txtbairro1.Text == "Seu bairro")
            {
                txtbairro1.Text = string.Empty;
            }
        }

        private void txtnome1_Click(object sender, EventArgs e)
        {
            if (txtnome1.Text == "Nome Completo")
            {
                txtnome1.Text = string.Empty;
            }
        }

        private void txtnumero1_Click(object sender, EventArgs e)
        {
            if (txtnumero1.Text == "Nº")
            {
                txtnumero1.Text = string.Empty;
            }
        }

        private void txtrua1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtbairro1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }
    }
}
