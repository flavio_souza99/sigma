﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Cadastrar_Cliente
{
    class ClienteBusiness
    {
        public int Salvar(ClienteDTO dto)
        {
            ClienteDataBase db = new ClienteDataBase();
            return db.Salvar(dto);
        }

        public void Alterar(ClienteDTO dto)
        {
            ClienteDataBase db = new ClienteDataBase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            ClienteDataBase db = new ClienteDataBase();
            db.Remover(id);
        }

        public List<ClienteDTO> Consultar(string produto)
        {
            ClienteDataBase db = new ClienteDataBase();
            return db.Consultar(produto);
        }

        public List<ClienteDTO> Listar()
        {
            ClienteDataBase db = new ClienteDataBase();
            return db.Listar();
        }
    }
}
