﻿using frmLogin.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Cadastrar_Cliente
{
    class ClienteDataBase
    {
        public int Salvar(ClienteDTO dto)
        {
            string script = @"INSERT INTO tb_cliente (nm_nome, ds_cpf, ds_nascimento, nu_telefone, nm_rua, nm_bairro, nu_numero, ds_cep, ds_sexo )
                                              VALUES (@nm_nome, @ds_cpf, @ds_nascimento, @nu_telefone, @nm_rua, @nm_bairro, @nu_numero, @ds_cep, @ds_sexo)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("ds_cpf", dto.Cpf));
            parms.Add(new MySqlParameter("ds_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("nu_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("nm_rua", dto.Rua));
            parms.Add(new MySqlParameter("nm_bairro", dto.Bairro));
            parms.Add(new MySqlParameter("nu_numero", dto.Numero));
            parms.Add(new MySqlParameter("ds_cep", dto.Cep));
            parms.Add(new MySqlParameter("ds_sexo", dto.Sexo));
           

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(ClienteDTO dto)
        {
            string script = @"UPDATE tb_cliente 
                                 SET nm_nome = @nm_nome,
                                     ds_cpf  = @ds_cpf,
                                     ds_nascimento = @nascimento,
                                     nu_telefone = @nu_telefone,
                                     nm_rua = @nm_rua,
                                     nm_bairro = @nm_bairro,
                                     nu_numero = @nu_numero,
                                     ds_cep =  @ds_cep,
                                     ds_sexo = @ds_sexo
                               WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.Id));
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("ds_cpf", dto.Cpf));
            parms.Add(new MySqlParameter("ds_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("nu_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("nm_rua", dto.Rua));
            parms.Add(new MySqlParameter("nm_bairro", dto.Bairro));
            parms.Add(new MySqlParameter("nu_numero", dto.Numero));
            parms.Add(new MySqlParameter("ds_cep", dto.Cep));
            parms.Add(new MySqlParameter("ds_sexo", dto.Sexo));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_cliente WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ClienteDTO> Consultar(string cliente)
        {
            string script = @"SELECT * FROM tb_cliente WHERE nm_nome like @nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", cliente + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_nome");
                dto.Cpf = reader.GetString("ds_cpf");
                dto.Nascimento = reader.GetDateTime("ds_nascimento");
                dto.Telefone = reader.GetString("nu_telefone");
                dto.Rua = reader.GetString("nm_rua");
                dto.Bairro = reader.GetString("nm_bairro");
                dto.Numero = reader.GetString("nu_numero");
                dto.Cep = reader.GetString("ds_cep");
                dto.Sexo = reader.GetString("ds_sexo");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<ClienteDTO> Listar()
        {
            string script = @"SELECT * FROM tb_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_nome");
                dto.Cpf = reader.GetString("ds_cpf");
                dto.Nascimento = reader.GetDateTime("ds_nascimento");
                dto.Telefone = reader.GetString("nu_telefone");
                dto.Rua = reader.GetString("nm_rua");
                dto.Bairro = reader.GetString("nm_bairro");
                dto.Numero = reader.GetString("nu_numero");
                dto.Cep = reader.GetString("ds_cep");
                dto.Sexo = reader.GetString("ds_sexo");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
