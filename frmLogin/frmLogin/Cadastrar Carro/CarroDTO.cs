﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Cadastrar_Carro
{
    class CarroDTO
    {
        public int Id { get; set; }
        public string Carro { get; set; }
        public string Placa { get; set; }
        public string Dono { get; set; }
    }
}
