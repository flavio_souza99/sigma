﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Cadastrar_Carro
{
    class CarroBusiness
    {
        public int Salvar(CarroDTO dto)
        {
            CarroDataBase db = new CarroDataBase();
            return db.Salvar(dto);
        }

        public void Alterar(CarroDTO dto)
        {
            CarroDataBase db = new CarroDataBase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            CarroDataBase db = new CarroDataBase();
            db.Remover(id);
        }

        public List<CarroDTO> Consultar(string produto)
        {
            CarroDataBase db = new CarroDataBase();
            return db.Consultar(produto);
        }

        public List<CarroDTO> Listar()
        {
            CarroDataBase db = new CarroDataBase();
            return db.Listar();
        }
    }
}
