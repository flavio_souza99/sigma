﻿using frmLogin.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Cadastrar_Carro
{
    class CarroDataBase
    {
        public int Salvar(CarroDTO dto)
        {
            string script = @"INSERT INTO tb_produto (nm_carro, ds_placa, nm_dono) VALUES (@nm_carro, @ds_placa, @nm_dono)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_carro", dto.Carro));
            parms.Add(new MySqlParameter("ds_placa", dto.Placa));
            parms.Add(new MySqlParameter("nm_dono", dto.Dono));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(CarroDTO dto)
        {
            string script = @"UPDATE tb_produto 
                                 SET nm_carro = @nm_carro,
                                     ds_placa   = @ds_placa,
                                     nm_dono   = @nm_dono
                               WHERE id_carro = @id_carro";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_carro", dto.Id));
            parms.Add(new MySqlParameter("nm_carro", dto.Carro));
            parms.Add(new MySqlParameter("ds_placa", dto.Placa));
            parms.Add(new MySqlParameter("nm_dono", dto.Dono));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_produto WHERE id_produto = @id_carro";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_carro", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public List<CarroDTO> Consultar(string produto)
        {
            string script = @"SELECT * FROM tb_produto WHERE nm_carro like @nm_carro";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_carro", produto + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CarroDTO> lista = new List<CarroDTO>();
            while (reader.Read())
            {
                CarroDTO dto = new CarroDTO();
                dto.Id = reader.GetInt32("id_carro");
                dto.Carro = reader.GetString("nm_carro");
                dto.Placa = reader.GetString("ds_placa");
                dto.Dono = reader.GetString("nm_dono");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<CarroDTO> Listar()
        {
            string script = @"SELECT * FROM tb_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CarroDTO> lista = new List<CarroDTO>();
            while (reader.Read())
            {
                CarroDTO dto = new CarroDTO();
                dto.Id = reader.GetInt32("id_carro");
                dto.Carro = reader.GetString("nm_carro");
                dto.Placa = reader.GetString("ds_placa");
                dto.Dono = reader.GetString("nm_dono");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
