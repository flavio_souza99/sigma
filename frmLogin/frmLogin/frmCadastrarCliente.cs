﻿using frmLogin.Cadastrar_Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class frmCadastrarCliente : Form
    {
        public frmCadastrarCliente()
        {
            InitializeComponent();
        }

        private void txtNumero_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            DateTime Nascimento = Convert.ToDateTime(txtData.Text);

            ClienteDTO dto = new ClienteDTO();
            dto.Nome = txtNome.Text.Trim();
            dto.Cpf = txtCpf.Text.Trim();
            dto.Nascimento = Nascimento;
            dto.Telefone = txtTelefone.Text.Trim();
            dto.Rua = txtRua.Text.Trim();
            dto.Bairro = txtBairro.Text.Trim();
            dto.Numero = txtNumero.Text.Trim();
            dto.Cep = txtCep.Text.Trim();
            dto.Sexo = txtSexo.Text.Trim();


            if (dto.Nome == string.Empty || dto.Nome == "Digite seu nome")
            {
                MessageBox.Show("Digite o Nome");

                return;
            }

            if (dto.Cpf == string.Empty || dto.Cpf == "Digitar cpf" || txtCpf.Text.Length <= 11)
            {
                MessageBox.Show("Digite o Cpf corretamente");

                return;
            }

            if (dto.Telefone == string.Empty || dto.Telefone == "Digitar o celular" || txtTelefone.Text.Length <= 14)
            {
                MessageBox.Show("Digite o Telefone");

                return;
            }

            if (dto.Rua == string.Empty || dto.Rua == "Rua")
            {
                MessageBox.Show("Digite a  Rua");

                return;
            }

            if (dto.Bairro == string.Empty || dto.Bairro == "Bairro")
            {
                MessageBox.Show("Digite o Bairro");

                return;
            }

            if (dto.Numero == string.Empty || dto.Numero == "Nº")
            {
                MessageBox.Show("Digite o Numero");

                return;
            }

            if (dto.Cep == string.Empty || dto.Cep == "Digitar cep" || txtCep.Text.Length <=7)
            {
                MessageBox.Show("Digite o Cep");

                return;
            }

            if (dto.Sexo == string.Empty || dto.Sexo == "Digitar Sexo")
            {
                MessageBox.Show("Digite o Sexo");

                return;
            }

            ClienteBusiness business = new ClienteBusiness();
            business.Salvar(dto);

            MessageBox.Show("Cliente salvo com sucesso.", "Sigma", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmCadastrarCliente_Load(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void txtData_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txtCep_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtCpf_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtTelefone_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtBairro_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtRua_TextChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtSexo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtNome_Click(object sender, EventArgs e)
        {
            if (txtNome.Text == "Digite seu nome")
            {
                txtNome.Text = string.Empty;
            }
        }

        private void txtRua_Click(object sender, EventArgs e)
        {
            if (txtRua.Text == "Rua")
            {
                txtRua.Text = string.Empty;
            }
        }

        private void txtNumero_Click(object sender, EventArgs e)
        {
            if (txtNumero.Text == "Nº")
            {
                txtNumero.Text = string.Empty;
            }
        }

        private void txtBairro_Click(object sender, EventArgs e)
        {
            if (txtBairro.Text == "Bairro")
            {
                txtBairro.Text = string.Empty;
            }
        }

        private void txtRua_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }
    }
}
