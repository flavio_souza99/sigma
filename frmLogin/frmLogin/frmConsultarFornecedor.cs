﻿using frmLogin.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class frmConsultarFornecedor : Form
    {
        public frmConsultarFornecedor()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Consultar(txtFuncionario.Text);

            dgvFornecedor.AutoGenerateColumns = false;
            dgvFornecedor.DataSource = lista;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
