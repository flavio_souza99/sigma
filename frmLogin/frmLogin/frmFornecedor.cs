﻿using frmLogin.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class frmFornecedor : Form
    {
        public frmFornecedor()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FornecedorDTO dto = new FornecedorDTO();
            dto.Nome = txtNome.Text.Trim();
            dto.Cnpj = txtCNPJ.Text.Trim();
            dto.Telefone = txtTelefone.Text.Trim();
            dto.Rua = txtRua.Text.Trim();
            dto.Bairro = txtBairro.Text.Trim();
            dto.CadastroFornecedor = txtCadastroFornecedor.Text.Trim();
            dto.Email = txtEmail.Text.Trim();
            dto.DenominacaoSocial = txtDenominacaoSocial.Text.Trim();
            dto.RamoAtividade = txtRamoAtividade.Text.Trim();
            dto.Funcao = txtFuncao.Text.Trim();
            dto.Entrega = txtEntrega.Text.Trim();
            dto.Desconto = txtDesconto.Text.Trim();



            if (dto.Nome == string.Empty || dto.Nome == "Nome")
            {
                MessageBox.Show("Digite o Nome");

                return;
            }
            if (dto.CadastroFornecedor == string.Empty || dto.CadastroFornecedor == "Nº" || txtCadastroFornecedor.Text.Length <= 10)
            {
                MessageBox.Show("Digite o cadastro de fornecedor");

                return;
            }
            if (dto.DenominacaoSocial == string.Empty || dto.DenominacaoSocial == "Denominação do fornecedor")
            {
                MessageBox.Show("Digite a denominacao social");

                return;
            }
            if (dto.Rua == string.Empty || dto.Rua == "Rua")
            {
                MessageBox.Show("Digite a rua");

                return;
            }


            if (dto.Bairro == string.Empty || dto.Bairro == "Bairro do fornecedor")
            {
                MessageBox.Show("Digite o bairro");

                return;
            }

            if (dto.Email == string.Empty || dto.Email == "Digite seu e-mail")
            {
                MessageBox.Show("Digite o Email");

                return;
            }

            if (dto.Cnpj == string.Empty || dto.Cnpj == "Digite o Cnpj" || txtCNPJ.Text.Length <=17)
            {
                MessageBox.Show("Digite o CNPJ");

                return;
            }
            if (dto.Inscricao == string.Empty || dto.Inscricao == "Nº")
            {
                MessageBox.Show("Digite sua Inscricao");

                return;
            }

            if (dto.Telefone == string.Empty || dto.Telefone == "Digitar telefone" || txtTelefone.Text.Length <= 13)
            {
                MessageBox.Show("Digite o Telefone");

                return;
            }


            if (dto.RamoAtividade == string.Empty || dto.RamoAtividade == "Qual o ramo do fornecedor")
            {
                MessageBox.Show("Digite o ramo de atividade");

                return;
            }

            if (dto.Funcao == string.Empty || dto.Funcao == "Função do fornecedor")
            {
                MessageBox.Show("Digite a funcao");

                return;
            }
            if (dto.Entrega == string.Empty || dto.Entrega == "A frequente data de entrega")
            {
                MessageBox.Show("Digite a prazo médio de entrega");

                return;
            }
            if (dto.Desconto == string.Empty || dto.Desconto == "Desconto proposto")
            {
                MessageBox.Show("Digite o desconto");

                return;
            }

            FornecedorBusiness business = new FornecedorBusiness();
            business.Salvar(dto);

            MessageBox.Show("Fornecedor salvo com sucesso.", "Peças", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnConsultarFornecedor_Click(object sender, EventArgs e)
        {
            frmConsultarFornecedor ir = new frmConsultarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtUF_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtNome_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtUF_KeyPress_1(object sender, KeyPressEventArgs e)
        {
              if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtBairro_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDesconto_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDesconto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar) || char.IsSymbol(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtNome_Click(object sender, EventArgs e)
        {
            if (txtNome.Text == "Nome")
            {
                txtNome.Text = string.Empty;
            }
        }

        private void txtDenominacaoSocial_Click(object sender, EventArgs e)
        {
            if (txtDenominacaoSocial.Text == "Denominação do fornecedor")
            {
                txtDenominacaoSocial.Text = string.Empty;
            }
        }

        private void txtRua_Click(object sender, EventArgs e)
        {
            if (txtRua.Text == "Rua")
            {
                txtRua.Text = string.Empty;
            }
        }

        private void txtBairro_Click(object sender, EventArgs e)
        {
            if (txtBairro.Text == "Bairro do fornecedor")
            {
                txtBairro.Text = string.Empty;
            }
        }

        private void txtFuncao_Click(object sender, EventArgs e)
        {
            if (txtFuncao.Text == "Função do fornecedor")
            {
                txtFuncao.Text = string.Empty;
            }
        }

        private void txtEmail_Click(object sender, EventArgs e)
        {
            if (txtEmail.Text == "Digite seu e-mail")
            {
                txtEmail.Text = string.Empty;
            }
        }

        private void txtRamoAtividade_Click(object sender, EventArgs e)
        {
            if (txtRamoAtividade.Text == "Qual o ramo do fornecedor")
            {
                txtRamoAtividade.Text = string.Empty;
            }
        }

        private void txtEntrega_Click(object sender, EventArgs e)
        {
            if (txtEntrega.Text == "A frequente data de entrega")
            {
                txtEntrega.Text = string.Empty;
            }
        }

        private void txtRamoAtividade_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtEntrega_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtRua_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }
    }
}
