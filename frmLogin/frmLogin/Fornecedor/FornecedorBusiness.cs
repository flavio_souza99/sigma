﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Fornecedor
{
    class FornecedorBusiness
    {
        public int Salvar(FornecedorDTO dto)
        {
            FornecedorDatabase db = new FornecedorDatabase();
            return db.Salvar(dto);
        }

        public void Alterar(FornecedorDTO dto)
        {
            FornecedorDatabase db = new FornecedorDatabase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            FornecedorDatabase db = new FornecedorDatabase();
            db.Remover(id);
        }

        public List<FornecedorDTO> Consultar(string produto)
        {
            FornecedorDatabase db = new FornecedorDatabase();
            return db.Consultar(produto);
        }

        public List<FornecedorDTO> Listar()
        {
            FornecedorDatabase db = new FornecedorDatabase();
            return db.Listar();
        }
    }
}
