﻿using frmLogin.Cadastrar_Carro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class frmCadastrarCarro : Form
    {
        public frmCadastrarCarro()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            CarroDTO dto = new CarroDTO();
            dto.Carro = txtCarro.Text.Trim();
            dto.Placa = txtPlaca.Text.Trim();
            dto.Dono = txtDono.Text.Trim();

            if (dto.Carro == string.Empty || dto.Carro == "Digitar o Carro")
            {
                MessageBox.Show("Digite o Carro");

                return;
            }

            if (dto.Placa == string.Empty || dto.Placa == "Digitar o Placa" || txtPlaca.Text.Length <=7)
            {
                MessageBox.Show("Digite a Placa");

                return;
            }

            if (dto.Dono == string.Empty || dto.Dono == "Digitar o Proprietário")
            {
                MessageBox.Show("Digite a Proprietário");

                return;
            }

            CarroBusiness business = new CarroBusiness();
            business.Salvar(dto);

            MessageBox.Show("Carro salvo com sucesso.", "Sigma", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void txtDono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void frmCadastrarCarro_Load(object sender, EventArgs e)
        {

        }

        private void txtDono_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtDono_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCarro_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
