﻿namespace frmLogin
{
    partial class frmCadastrar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button5 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblclose1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtsenha = new System.Windows.Forms.TextBox();
            this.txtnome1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtsexo1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtdepartamento1 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txttel1 = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtrua1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtbairro1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtcep1 = new System.Windows.Forms.MaskedTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtrg1 = new System.Windows.Forms.MaskedTextBox();
            this.txtnumero1 = new System.Windows.Forms.TextBox();
            this.txtnascimento1 = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Black;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(694, 19);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(27, 26);
            this.button5.TabIndex = 50;
            this.button5.Text = "←";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(727, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 36);
            this.label1.TabIndex = 49;
            this.label1.Text = "-";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblclose1
            // 
            this.lblclose1.AutoSize = true;
            this.lblclose1.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblclose1.Location = new System.Drawing.Point(758, 9);
            this.lblclose1.Name = "lblclose1";
            this.lblclose1.Size = new System.Drawing.Size(29, 36);
            this.lblclose1.TabIndex = 48;
            this.lblclose1.Text = "x";
            this.lblclose1.Click += new System.EventHandler(this.lblclose1_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Black;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(12, 428);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(775, 36);
            this.button3.TabIndex = 47;
            this.button3.Text = "Cadastrar";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtnome
            // 
            this.txtnome.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnome.Location = new System.Drawing.Point(12, 158);
            this.txtnome.MaxLength = 100;
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(306, 23);
            this.txtnome.TabIndex = 44;
            this.txtnome.Text = "Crie um E-mail";
            this.txtnome.Click += new System.EventHandler(this.txtnome_Click);
            this.txtnome.TextChanged += new System.EventHandler(this.txtnome_TextChanged);
            this.txtnome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnome_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(124, 124);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 22);
            this.label11.TabIndex = 43;
            this.label11.Text = "Email:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Jokerman", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(152, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(485, 55);
            this.label8.TabIndex = 42;
            this.label8.Text = "Cadastro de Funcionários";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(124, 195);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 22);
            this.label14.TabIndex = 45;
            this.label14.Text = "Senha:";
            // 
            // txtsenha
            // 
            this.txtsenha.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsenha.Location = new System.Drawing.Point(12, 220);
            this.txtsenha.MaxLength = 100;
            this.txtsenha.Name = "txtsenha";
            this.txtsenha.Size = new System.Drawing.Size(306, 26);
            this.txtsenha.TabIndex = 46;
            this.txtsenha.Text = "qualquer";
            this.txtsenha.UseSystemPasswordChar = true;
            this.txtsenha.Click += new System.EventHandler(this.txtsenha_Click);
            this.txtsenha.TextChanged += new System.EventHandler(this.txtsenha_TextChanged);
            this.txtsenha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtsenha_KeyPress);
            // 
            // txtnome1
            // 
            this.txtnome1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnome1.Location = new System.Drawing.Point(424, 158);
            this.txtnome1.MaxLength = 100;
            this.txtnome1.Name = "txtnome1";
            this.txtnome1.Size = new System.Drawing.Size(363, 23);
            this.txtnome1.TabIndex = 51;
            this.txtnome1.Text = "Nome Completo";
            this.txtnome1.Click += new System.EventHandler(this.txtnome1_Click);
            this.txtnome1.TextChanged += new System.EventHandler(this.txtnome1_TextChanged);
            this.txtnome1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnome1_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(348, 156);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 22);
            this.label2.TabIndex = 52;
            this.label2.Text = "Nome:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(348, 224);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 22);
            this.label3.TabIndex = 53;
            this.label3.Text = "RG:";
            // 
            // txtsexo1
            // 
            this.txtsexo1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtsexo1.FormattingEnabled = true;
            this.txtsexo1.Items.AddRange(new object[] {
            "Masculino",
            "Feminino"});
            this.txtsexo1.Location = new System.Drawing.Point(424, 333);
            this.txtsexo1.Name = "txtsexo1";
            this.txtsexo1.Size = new System.Drawing.Size(116, 21);
            this.txtsexo1.TabIndex = 55;
            this.txtsexo1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(348, 329);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 22);
            this.label4.TabIndex = 56;
            this.label4.Text = "Sexo:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(90, 260);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(153, 22);
            this.label5.TabIndex = 58;
            this.label5.Text = "Departamento:";
            // 
            // txtdepartamento1
            // 
            this.txtdepartamento1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtdepartamento1.FormattingEnabled = true;
            this.txtdepartamento1.Items.AddRange(new object[] {
            "Mecanico",
            "Administrador",
            "Atendente"});
            this.txtdepartamento1.Location = new System.Drawing.Point(12, 285);
            this.txtdepartamento1.Name = "txtdepartamento1";
            this.txtdepartamento1.Size = new System.Drawing.Size(306, 21);
            this.txtdepartamento1.TabIndex = 57;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(568, 278);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 22);
            this.label6.TabIndex = 59;
            this.label6.Text = "Celular:";
            // 
            // txttel1
            // 
            this.txttel1.Location = new System.Drawing.Point(685, 282);
            this.txttel1.Mask = "(99) 99999-9999";
            this.txttel1.Name = "txttel1";
            this.txttel1.Size = new System.Drawing.Size(102, 20);
            this.txttel1.TabIndex = 60;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(572, 224);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(124, 22);
            this.label7.TabIndex = 61;
            this.label7.Text = "Nascimento:";
            // 
            // txtrua1
            // 
            this.txtrua1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrua1.Location = new System.Drawing.Point(79, 328);
            this.txtrua1.MaxLength = 100;
            this.txtrua1.Name = "txtrua1";
            this.txtrua1.Size = new System.Drawing.Size(239, 23);
            this.txtrua1.TabIndex = 64;
            this.txtrua1.Text = "Sua rua";
            this.txtrua1.Click += new System.EventHandler(this.txtrua1_Click);
            this.txtrua1.TextChanged += new System.EventHandler(this.txtrua1_TextChanged);
            this.txtrua1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtrua1_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 326);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 22);
            this.label9.TabIndex = 63;
            this.label9.Text = "Rua:";
            // 
            // txtbairro1
            // 
            this.txtbairro1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbairro1.Location = new System.Drawing.Point(79, 374);
            this.txtbairro1.MaxLength = 100;
            this.txtbairro1.Name = "txtbairro1";
            this.txtbairro1.Size = new System.Drawing.Size(239, 23);
            this.txtbairro1.TabIndex = 66;
            this.txtbairro1.Text = "Seu bairro";
            this.txtbairro1.Click += new System.EventHandler(this.txtbairro1_Click);
            this.txtbairro1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbairro1_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(12, 372);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 22);
            this.label10.TabIndex = 65;
            this.label10.Text = "Bairro:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(348, 282);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(51, 22);
            this.label12.TabIndex = 67;
            this.label12.Text = "CEP:";
            // 
            // txtcep1
            // 
            this.txtcep1.Location = new System.Drawing.Point(424, 285);
            this.txtcep1.Mask = "99999-999";
            this.txtcep1.Name = "txtcep1";
            this.txtcep1.Size = new System.Drawing.Size(62, 20);
            this.txtcep1.TabIndex = 68;
            this.txtcep1.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtcep1_MaskInputRejected);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(572, 329);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 22);
            this.label13.TabIndex = 69;
            this.label13.Text = "Numero:";
            // 
            // txtrg1
            // 
            this.txtrg1.Location = new System.Drawing.Point(424, 225);
            this.txtrg1.Mask = "99.999.999-9";
            this.txtrg1.Name = "txtrg1";
            this.txtrg1.Size = new System.Drawing.Size(74, 20);
            this.txtrg1.TabIndex = 71;
            // 
            // txtnumero1
            // 
            this.txtnumero1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnumero1.Location = new System.Drawing.Point(685, 333);
            this.txtnumero1.MaxLength = 100;
            this.txtnumero1.Name = "txtnumero1";
            this.txtnumero1.Size = new System.Drawing.Size(102, 23);
            this.txtnumero1.TabIndex = 72;
            this.txtnumero1.Text = "Nº";
            this.txtnumero1.Click += new System.EventHandler(this.txtnumero1_Click);
            this.txtnumero1.TextChanged += new System.EventHandler(this.txtnumero1_TextChanged);
            // 
            // txtnascimento1
            // 
            this.txtnascimento1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtnascimento1.Location = new System.Drawing.Point(702, 226);
            this.txtnascimento1.Name = "txtnascimento1";
            this.txtnascimento1.Size = new System.Drawing.Size(83, 20);
            this.txtnascimento1.TabIndex = 74;
            this.txtnascimento1.Value = new System.DateTime(1999, 7, 22, 13, 35, 0, 0);
            this.txtnascimento1.ValueChanged += new System.EventHandler(this.kkk_ValueChanged);
            // 
            // frmCadastrar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(806, 500);
            this.Controls.Add(this.txtnascimento1);
            this.Controls.Add(this.txtnumero1);
            this.Controls.Add(this.txtrg1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtcep1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtbairro1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtrua1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txttel1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtdepartamento1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtsexo1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtnome1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblclose1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtsenha);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmCadastrar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmCadastrar";
            this.Load += new System.EventHandler(this.frmCadastrar_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblclose1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtsenha;
        private System.Windows.Forms.TextBox txtnome1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox txtsexo1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox txtdepartamento1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox txttel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtrua1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtbairro1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.MaskedTextBox txtcep1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox txtrg1;
        private System.Windows.Forms.TextBox txtnumero1;
        private System.Windows.Forms.DateTimePicker txtnascimento1;
    }
}