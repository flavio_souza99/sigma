﻿using frmLogin.Orçamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class frmConsultarOrcamento : Form
    {
        public frmConsultarOrcamento()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            OrcamentoBusiness business = new OrcamentoBusiness();
            List<OrcamentoConsultarView> lista = business.Consultar(txtCliente.Text);

            dgvPedido.AutoGenerateColumns = false;
            dgvPedido.DataSource = lista;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void frmConsultarOrcamento_Load(object sender, EventArgs e)
        {

        }

        private void dgvPedido_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                OrcamentoConsultarView pedido = dgvPedido.Rows[e.RowIndex].DataBoundItem as OrcamentoConsultarView;

                DialogResult r = MessageBox.Show($"Deseja realmente excluir o pedido do orcamento? {pedido.Id}?", "Sigma",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    OrcamentoBusiness business = new OrcamentoBusiness();
                    business.Remover(pedido.Id);

                    button4_Click(null, null);
                }
            }
        }

        private void dgvPedido_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
