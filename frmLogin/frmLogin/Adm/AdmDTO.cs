﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Adm
{
    class AdmDTO
    {
        public int Id { get; set; }
        public string chave { get; set; }
        public string passe { get; set; }
    }
}
