﻿using frmLogin.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Adm
{
    class AdmDataBase
    {

        public bool Logar(string chave, string passe)
        {
            string script =
                              @"SELECT *  
                              FROM Adm 
                              where chave = @chave and
                                           passe = @passe";

            List<MySqlParameter> parametros = new List<MySqlParameter>();
            parametros.Add(new MySqlParameter("chave", chave));
            parametros.Add(new MySqlParameter("passe", passe));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parametros);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
