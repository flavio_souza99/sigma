﻿using frmLogin.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Login
{
    class LoginDataBase
    {
        public bool Logar(string nome, string senha)
        {
            string script =
                              @"SELECT *  
                              FROM logar 
                              where nome = @nome and
                                           senha = @senha";

            List<MySqlParameter> parametros = new List<MySqlParameter>();
            parametros.Add(new MySqlParameter("nome", nome));
            parametros.Add(new MySqlParameter("senha", senha));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parametros);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public int Salvar(loginDTO dados)
        {
            string script =
             @"INSERT INTO logar (nome, senha, nome1, rg1, sexo1, tel1, cep1, nascimento1, numero1, departamento1, bairro1, rua1)
                          VALUES (@nome, @senha, @nome1, @rg1, @sexo1, @tel1, @cep1, @nascimento1, @numero1, @departamento1, @bairro1, @rua1)";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("nome", dados.nome));
            parms.Add(new MySqlParameter("senha", dados.senha));
            parms.Add(new MySqlParameter("nome1", dados.nome1));
            parms.Add(new MySqlParameter("rg1", dados.rg1));
            parms.Add(new MySqlParameter("sexo1", dados.sexo1));
            parms.Add(new MySqlParameter("tel1", dados.tel1));
            parms.Add(new MySqlParameter("cep1", dados.cep1));
            parms.Add(new MySqlParameter("nascimento1", dados.nascimento1));
            parms.Add(new MySqlParameter("numero1", dados.numero1));
            parms.Add(new MySqlParameter("departamento1", dados.departamento1));
            parms.Add(new MySqlParameter("bairro1", dados.bairro1));
            parms.Add(new MySqlParameter("rua1", dados.rua1));



            DataBase db = new DataBase();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<loginDTO> Consultar(string nome)
        {
            string script = @"SELECT * FROM logar WHERE nome like @nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nome", nome + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<loginDTO> lista = new List<loginDTO>();
            while (reader.Read())
            {
                loginDTO dto = new loginDTO();
                dto.Id = reader.GetInt32("id_log");
                dto.nome = reader.GetString("nome");
                dto.senha = reader.GetString("senha");
                dto.nome1 = reader.GetString("nome1");
                dto.rg1 = reader.GetString("rg1");
                dto.sexo1 = reader.GetString("sexo1");
                dto.tel1 = reader.GetString("tel1");
                dto.cep1 = reader.GetString("cep1");
                dto.nascimento1 = reader.GetDateTime("nascimento1");
                dto.numero1 = reader.GetString("numero1");
                dto.departamento1 = reader.GetString("departamento1");
                dto.bairro1 = reader.GetString("bairro1");
                dto.rua1 = reader.GetString("rua1");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
