﻿drop database if exists login;

create database Login;

use Login;

-- CRIA A TABELA Logar
create table logar( id_log integer primary key auto_increment, 
nome varchar (100),
senha varchar (100),
nome1 varchar (100),
rg1 varchar (100),
sexo1 varchar (100),
tel1 varchar (100),
cep1 varchar (100),
nascimento1 datetime,
numero1 varchar (100),
departamento1 varchar (100),
bairro1 varchar (100),
rua1 varchar (100));

insert into logar (nome, senha, nome1, rg1, sexo1, tel1, cep1, nascimento1, numero1, departamento1, bairro1, rua1)
values ('adm', '1234', 'Marcos Renato Alves', '32.036.125-8', 'Masculino', '(11) 95635-1578', '04857-250', '1986/05/22', 'Nº 35', 'Administrador', 'Jd. Varginha', 'Dos DBA');


-- CRIA A TABELA ADM
create table Adm ( id_adm int primary key auto_increment,
chave varchar(100),
passe varchar(50));

insert into Adm (chave, passe)
values ('sigma', '1234');

-- CRIA A TABELA Cliente
create table tb_cliente ( id_cliente int primary key auto_increment,
nm_nome varchar(100),
ds_cpf varchar(20),
ds_nascimento datetime,
nu_telefone varchar(20),
nm_rua varchar(100),
nm_bairro varchar(100),
nu_numero varchar(100),
ds_cep varchar(20),
ds_sexo varchar(50));

-- CRIA A TABELA PRODUTO
create table tb_produto ( id_carro int primary key auto_increment,
nm_carro varchar(100),
ds_placa varchar(50),
nm_dono varchar(100));

insert into tb_produto (nm_carro, ds_placa, nm_dono)
values ('lamborghini aventador', 'DER-1234', 'Marcos Renato Alves');

-- CRIA A TABELA fornecedor
create table tb_fornecedor ( id_cliente int primary key auto_increment,
nm_nome varchar(100),
ds_cnpj varchar(20),
ds_cadastrofornecedor varchar(50),
nr_telefone varchar(20),
nm_rua varchar(100),
nm_bairro varchar(100),
ds_email varchar(100),
ds_denominacaosocial varchar(50),
ds_ramoatividade varchar(80),
ds_funcao varchar (30),
ds_entrega varchar(30),
ds_desconto varchar (15));


-- CRIA A TABELA Pedido
create table tb_pedido ( id_pedido int primary key auto_increment,
id_carro int,
id_cliente int,
nm_valor1 varchar(50),
nm_valor2 varchar(50),
nm_valor3 varchar(50),
nm_defeito varchar(300),
foreign key (id_carro) references tb_produto (id_carro),
foreign key (id_cliente) references tb_cliente (id_cliente));



CREATE VIEW vw_pedido_consultar AS 
	SELECT tb_pedido.id_pedido,
		   tb_cliente.nm_nome,
		   tb_produto.nm_carro,
           tb_pedido.nm_valor1,
           tb_pedido.nm_valor2,
           tb_pedido.nm_valor3,
		   tb_pedido.nm_defeito		   
      FROM tb_pedido
	  JOIN tb_produto
        ON tb_pedido.id_carro = tb_produto.id_carro
	  JOIN tb_cliente
        ON tb_pedido.id_cliente = tb_cliente.id_cliente;
	 
		  
select * from logar;
select * from tb_cliente;
select * from tb_produto;
select * from tb_fornecedor;
select * from vw_pedido_consultar;

            
         



            
         