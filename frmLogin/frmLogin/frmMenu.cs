﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void frmMenu_Load(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmlogar retroceder = new frmlogar();
            this.Hide();
            retroceder.ShowDialog();
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmCadastrarCliente ir = new frmCadastrarCliente();
            this.Hide();
            ir.ShowDialog();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            frmConsultarFuncionario ir = new frmConsultarFuncionario();
            this.Hide();
            ir.ShowDialog();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            frmConsultarCliente ir = new frmConsultarCliente();
            this.Hide();
            ir.ShowDialog();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            frmCadastrarCarro ir = new frmCadastrarCarro();
            this.Hide();
            ir.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmConsultarCarro ir = new frmConsultarCarro();
            this.Hide();
            ir.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            frmOrcamento ir = new frmOrcamento();
            this.Hide();
            ir.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            frmConsultarOrcamento ir = new frmConsultarOrcamento();
            this.Hide();
            ir.ShowDialog();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Calculadora ir = new Calculadora();
            this.Hide();
            ir.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            frmFornecedor ir = new frmFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Sobre ir = new Sobre();
            this.Hide();
            ir.ShowDialog();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            frmConsultarFornecedor ir = new frmConsultarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
