﻿using frmLogin.Cadastrar_Carro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class frmConsultarCarro : Form
    {
        public frmConsultarCarro()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            CarroBusiness business = new CarroBusiness();
            List<CarroDTO> lista = business.Consultar(txtCarro.Text);

            dgvCarros.AutoGenerateColumns = false;
            dgvCarros.DataSource = lista;
        }

        private void dgvCarros_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
