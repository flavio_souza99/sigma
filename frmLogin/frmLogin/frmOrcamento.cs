﻿using frmLogin.Cadastrar_Carro;
using frmLogin.Cadastrar_Cliente;
using frmLogin.Orçamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class frmOrcamento : Form
    {
        BindingList<CarroDTO> produtosCarrinho = new BindingList<CarroDTO>();

        public frmOrcamento()
        {
            InitializeComponent();
            CarregarCombo();
            CarregarCombos();

        }

        void CarregarCombo()
        {
            CarroBusiness business = new CarroBusiness();
            List<CarroDTO> lista = business.Listar();

            cboCarro.ValueMember = nameof(CarroDTO.Id);
            cboCarro.DisplayMember = nameof(CarroDTO.Carro);
            cboCarro.DataSource = lista;
        }

        void CarregarCombos()
        {
            ClienteBusiness business = new ClienteBusiness();
            List<ClienteDTO> lista = business.Listar();

            cboCliente.ValueMember = nameof(ClienteDTO.Id);
            cboCliente.DisplayMember = nameof(ClienteDTO.Nome);
            cboCliente.DataSource = lista;
        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarroDTO dto = cboCarro.SelectedItem as CarroDTO;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            CarroDTO car = cboCarro.SelectedItem as CarroDTO;          
            ClienteDTO cli = cboCliente.SelectedItem as ClienteDTO;


            OrcamentoDTO dto = new OrcamentoDTO();
            dto.CarroId = car.Id;
            dto.ClienteId = cli.Id;
            dto.Valor1 = txtValor1.Text.Trim();
            dto.Valor2 = txtValor2.Text.Trim();
            dto.Valor3 = txtValor3.Text.Trim();
            dto.Defeito = txtDefeito.Text.Trim();


            if (txtValor1.Text == string.Empty || txtValor1.Text == "Nome Peça")
            {
                MessageBox.Show("O campo Peça é obrigatório", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;

            }

            if (txtValor2.Text == string.Empty || txtValor2.Text == "Mão de obra")
            {
                MessageBox.Show("O campo Mão de obra é obrigatório", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;

            }

            txtValor3.Text = Convert.ToString((Convert.ToDecimal(txtValor1.Text)) + (Convert.ToDecimal(txtValor2.Text)));
            dto.Valor3 = txtValor3.Text;
            OrcamentoBusiness business = new OrcamentoBusiness();
            business.Salvar(dto);
            MessageBox.Show("Orçamento salvo com sucesso.", "Sigma", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void frmOrcamento_Load(object sender, EventArgs e)
        {


        }

        private void txtValor1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtValor2_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtValor1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtValor2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void cboCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClienteDTO dto = cboCliente.SelectedItem as ClienteDTO;
        }
    }
}
